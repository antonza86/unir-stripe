const express = require('express');
require('dotenv').config();
const stripe = require('stripe')(process.env.SK_STRIPE);
const router = express.Router();

router.post('/checkWebHookStripe', async (req, res) => {
  try {
    console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    console.log(req.body);
    console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    console.log('Capture elements value');
    const data = req.body;
    console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    console.log(data.token);
    console.log(data.email);
    console.log(data.name);
    console.log(data.metadata);
    console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
    const customer = await stripe.customers.create({
      source: data.token,
      email: data.email,
      name: data.name,
      metadata: data.metadata
    });
    console.log(customer.id);
    const result = await stripe.paymentIntents.create({
      amount: (data.amount * 100), // Represented in cents
      currency: 'eur',
      customer: customer.id,
      receipt_email: data.email,
      confirm: true,
      payment_method_types: ['card'],
      // return_url: data.return_url ? data.return_url : '',
      metadata: data.metadata,
      description: data.description
    });
    console.log(result);
    if (result.next_action && result.next_action.use_stripe_sdk) {
      res.send(result);
    }
    if (result.status === 'succeeded') {
      res.send(true);
    } else {
      res.status(400).send(false);
    }
  } catch (error) {
    console.error(error);
    return false;
  }
});

module.exports = router;