const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt = require('jsonwebtoken');
require('dotenv').config();

app.use(bodyParser.json({limit: '50mb', extended: true}));

const paymentCtrllr = require('./controllers/payment');
app.use('/configs', cors(corsOptions), paymentCtrllr);


var corsOptions = {
  origin: process.env.CORS_URL,
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}


app.listen(process.env.LISTEN_PORT, () => {
  console.log('Connected to server');
});

// function authenticateToken(req, res, next) {
//   const authHeader = (req.headers && req.headers.authorization) ? req.headers.authorization.split(' ')[1] : '';
//   console.log(req.headers);
//   console.log(authHeader);
//   const token = authHeader;
//   if (!token) {
//     return res.status(401).json({
//       error: "There is no auth token in the request"
//     });
//   }
//   jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
//     if (err) {
//       return res.status(403).json({
//         error: err
//       });
//     }
//     req.user = user;
//     console.log('........');
//     console.log(req.user);
//     console.log('........');
//     next();
//   });
// }